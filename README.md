# Multilevel Modeling in R

These materials accompany the DUPRI "Multilevel Modeling in R" training. If you have any questions, contact Mark Yacoub at <mark.yacoub@duke.edu>.

* multilevel.Rmd - Rmarkdown HTML version of training code
* multilevel.html - HTML report of training for reference
* popular2.txt - Data set used for workshop
* Multilevel Modeling.Rproj - RStudio project file
